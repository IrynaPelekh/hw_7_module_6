use people_ua_db
go

drop table if exists person
go

create table person (
  id int not null identity,
  surname nvarchar(20) not null,
  name nvarchar(20) not null,
  sex nchar(1) not null,
  birthday date not null,
  people_region_id int not null,
  materniny_ward_id int not null,
  constraint PK_person primary key nonclustered(id)
)
go

drop table if exists region
go

create table region (
  id int not null identity,
  name nvarchar(20) not null,
  main_city nvarchar(20) not null,
  city_quantity int null,
  village_quantity int null,
  
  constraint PK_region primary key nonclustered(id)
)


drop table if exists materniny_ward
go

create table materniny_ward (
  id int not null identity,
  name nvarchar(50) not null,
  mw_region_id int not null,
  constraint PK_materniny_ward primary key(id)
)

drop table if exists person_materniny_ward
go

create table person_materniny_ward (
  id int not null identity,
  person_id int not null,
  materniny_ward_id int not null
  constraint PK_person_materniny_ward primary key(id)
)




