use [people_ua_db]
go

alter table person
add constraint FK_person_region
foreign key (people_region_id)
references region(id)
on delete cascade
on update cascade
go

alter table materniny_ward
add constraint FK_materniny_ward_region
foreign key(mw_region_id)
references region(id)
on delete cascade
on update cascade
go

alter table person_materniny_ward
add constraint FK_person_birth
foreign key(person_id)
references person(id)
on delete cascade
on update cascade
go

alter table person_materniny_ward
add constraint FK_mw_birth
foreign key(materniny_ward_id)
references materniny_ward(id)
go

